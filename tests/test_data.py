import os
import platform

from mdstat_influx.mdstat_influx import MdData

def test_single_raid5():

    md_data = {
        'devices': {
            'md0': {
                'active': True,
                'bitmap': None,
                'disks': {
                    'sdc1': {
                        'faulty': False,
                        'number': 0,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False
                    },
                    'sdd1': {
                        'faulty': False,
                        'number': 1,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False,
                    },
                    'sde1': {
                        'faulty': False,
                        'number': 3,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False,
                    },
                },
                'personality': 'raid5',
                'read_only': False,
                'resync': None,
                'status': {
                    'algorithm': 2,
                    'blocks': 7813770240,
                    'chunk_size': '512k',
                    'level': 5,
                    'non_degraded_disks': 3,
                    'raid_disks': 3,
                    'super': '1.2',
                    'synced': [True, True, True],
                },
            },
        },
        'personalities': [
            'raid6',
            'raid5',
            'raid4',
            'linear',
            'multipath',
            'raid0',
            'raid1',
            'raid10',
        ],
        'unused_devices': [],
    }

    md = MdData(md_data=md_data)

    assert len(md.devices) == 1
    assert len(md.personalities) == 8
    assert len(md.unused_devices) == 0

    for i in md_data['personalities']:
        assert i in md.personalities

    assert md.devices[0].name == 'md0'
    assert md.devices[0].active == True
    assert md.devices[0].personality == 'raid5'
    assert md.devices[0].personality in md.personalities
    assert md.devices[0].read_only == False

    assert md.devices[0].faulty_disk_cnt() == 0
    assert md.devices[0].replacement_disk_cnt() == 0
    assert md.devices[0].spare_disk_cnt() == 0

    msg = md.devices[0].line_message()
    print(msg)
    assert msg.find("disks=3i") != -1
    assert msg.find(platform.node()) != -1

    msgs = md.get_messages()
    print(msgs)
    assert msgs.find("disks=3i") != -1
    assert msgs.find(platform.node()) != -1

def test_dual_raid5():

    md_data = {
        'devices': {
            'md0': {
                'active': True,
                'bitmap': None,
                'disks': {
                    'sdc1': {
                        'faulty': False,
                        'number': 0,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False
                    },
                    'sdd1': {
                        'faulty': False,
                        'number': 1,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False,
                    },
                    'sde1': {
                        'faulty': False,
                        'number': 3,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False,
                    },
                },
                'personality': 'raid5',
                'read_only': False,
                'resync': None,
                'status': {
                    'algorithm': 2,
                    'blocks': 7813770240,
                    'chunk_size': '512k',
                    'level': 5,
                    'non_degraded_disks': 3,
                    'raid_disks': 3,
                    'super': '1.2',
                    'synced': [True, True, True],
                },
            },
            'md1': {
                'active': True,
                'bitmap': None,
                'disks': {
                    'sdf1': {
                        'faulty': False,
                        'number': 0,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False
                    },
                    'sdg1': {
                        'faulty': False,
                        'number': 1,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False,
                    },
                    'sdh1': {
                        'faulty': False,
                        'number': 3,
                        'replacement': False,
                        'spare': False,
                        'write_mostly': False,
                    },
                },
                'personality': 'raid5',
                'read_only': False,
                'resync': None,
                'status': {
                    'algorithm': 2,
                    'blocks': 7813770240,
                    'chunk_size': '512k',
                    'level': 5,
                    'non_degraded_disks': 3,
                    'raid_disks': 3,
                    'super': '1.2',
                    'synced': [True, True, True],
                },
            },
        },
        'personalities': [
            'raid6',
            'raid5',
            'raid4',
            'linear',
            'multipath',
            'raid0',
            'raid1',
            'raid10',
        ],
        'unused_devices': [],
    }

    md = MdData(md_data=md_data)

    assert len(md.devices) == 2
    assert len(md.personalities) == 8
    assert len(md.unused_devices) == 0

    for i in md_data['personalities']:
        assert i in md.personalities


    msgs = md.get_messages()
    print(msgs)
    assert msgs.find("device_name=md0") != -1
    assert msgs.find("device_name=md1") != -1
